package de.home.cbl.jobbotapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableMongoRepositories(basePackages = "de.home.cbl.jobbotapi.repository")
public class JobbotApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobbotApiApplication.class, args);
    }

}
