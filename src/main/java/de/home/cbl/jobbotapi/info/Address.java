package de.home.cbl.jobbotapi.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {
  private String street;
  private Integer streetNr;
  private String zip;
  private String city;
  private String country;
}
