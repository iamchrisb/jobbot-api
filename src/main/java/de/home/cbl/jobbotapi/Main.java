package de.home.cbl.jobbotapi;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.DAYS;

public class Main {
  public static void main(String[] args) {
    UUID userId = UUID.randomUUID();
    UUID projectId = UUID.randomUUID();
    String test = String.format("users/%s/images/projects/%s", userId, projectId);
    System.out.println(test);

    String testDateStr = "Oktober 18, 2019";

    DateTimeFormatter germanDateTimeFormatter =
        DateTimeFormatter.ofPattern("MMMM dd, yyyy", Locale.GERMAN);
    LocalDate requestDate = LocalDate.parse(testDateStr, germanDateTimeFormatter);
    System.out.println(requestDate);

    LocalDate today = LocalDate.now();
    long daysBetween = DAYS.between(requestDate, today);
    System.out.println("days: " + daysBetween);
  }
}
