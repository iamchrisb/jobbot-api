package de.home.cbl.jobbotapi.email;

import de.home.cbl.jobbotapi.utils.Constants;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {

  public static final String SERVER_PORT_KEY = "server.port";

  public static final String FROM_MAIL = "info@slashers.de";

  Logger logger = LoggerFactory.getLogger(EmailService.class);

  private String hostAddress;
  private String verificationUrl;
  private JavaMailSender javaMailSender;
  private Environment environment;
  private String protocol = "http://";

  public EmailService(
      @Autowired JavaMailSender javaMailSender, @Autowired Environment environment) {
    this.javaMailSender = javaMailSender;
    this.environment = environment;
  }

  @PostConstruct
  private void init() throws UnknownHostException {
    hostAddress = InetAddress.getLocalHost().getHostAddress();

    String appPort = environment.getRequiredProperty(SERVER_PORT_KEY);
    verificationUrl =
        protocol
            + hostAddress
            + ":"
            + appPort
            + Constants.ROUTE_AUTHENTICATIONS
            + Constants.VERIFY_EMAIL;
  }

  public void sendEmailVerificationSucceed(String userEmail) {
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom(FROM_MAIL);
    message.setTo(userEmail);
    message.setSubject("Account Verification ");
    message.setText("Hurray! Your user account is now verified!");
    try {
      javaMailSender.send(message);
    } catch (Exception e) {
      logger.info(e.getMessage());
    }
  }

  public void sendRequestEmailVerification(String userEmail, UUID verifyToken) {
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom(FROM_MAIL);
    message.setTo(userEmail);
    message.setSubject("Account Verification Requested");
    message.setText(
        "Hey there, you just requested to verify your user account. Please follow this link for activation: "
            + verificationUrl
            + "?token="
            + verifyToken);
    try {
      javaMailSender.send(message);
    } catch (Exception e) {
      logger.info(e.getMessage());
    }
  }
}
