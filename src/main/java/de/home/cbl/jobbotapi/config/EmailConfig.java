package de.home.cbl.jobbotapi.config;

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class EmailConfig {

  Logger logger = LoggerFactory.getLogger(EmailConfig.class);

  @Value("${twireEmailUsername}")
  private String emailUsername;

  @Value("${twireEmailPassword}")
  private String emailPassword;

  @Value("${twireEmailHost}")
  private String twireEmailHost;

  @Value("${twireEmailPort}")
  private int twireEmailPort;

  @Bean
  public JavaMailSender getJavaMailSender() {
    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
    javaMailSender.setHost(twireEmailHost);
    javaMailSender.setPort(twireEmailPort);

    javaMailSender.setUsername(emailUsername);
    javaMailSender.setPassword(emailPassword);

    Properties props = javaMailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.debug", "true");

    return javaMailSender;
  }
}
