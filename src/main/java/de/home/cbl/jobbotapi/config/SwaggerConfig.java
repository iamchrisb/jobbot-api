package de.home.cbl.jobbotapi.config;

import com.google.common.collect.Lists;
import com.google.common.net.HttpHeaders;
import de.home.cbl.jobbotapi.utils.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";
  private static final String KEY_NAME = "JWT";
  private static final String PASS_AS = "header";
  List<VendorExtension> vendorExtensions = new ArrayList<>();

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .securityContexts(Lists.newArrayList(securityContext()))
        .securitySchemes(Lists.newArrayList(apiKey()))
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build();
  }

  private ApiKey apiKey() {
    return new ApiKey(KEY_NAME, HttpHeaders.AUTHORIZATION, PASS_AS);
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder()
        .securityReferences(defaultAuth())
        .forPaths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
        .build();
  }

  List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Lists.newArrayList(new SecurityReference("JWT", authorizationScopes));
  }

  private ApiInfo apiInfo() {
    return new ApiInfo(
        Constants.API_TITLE,
        Constants.API_DESCRIPTION,
        Constants.API_VERSION,
        Constants.TERMS_OF_SERVICE_URL,
        contact(),
        Constants.API_LICENSE,
        Constants.API_LICENSE_URL,
        this.vendorExtensions);
  }

  private Contact contact() {
    return new Contact(Constants.CONTACT_PERSON, Constants.CONTACT_URL, Constants.CONTACT_EMAIL);
  }
}
