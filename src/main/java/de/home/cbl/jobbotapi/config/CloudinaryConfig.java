package de.home.cbl.jobbotapi.config;

import com.cloudinary.Cloudinary;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class CloudinaryConfig {

  @Value("${cloudinary.cloudName}")
  private String cloudName;

  @Value("${cloudinary.apiKey}")
  private String apiKey;

  @Value("${cloudinary.apiSecret}")
  private String apiSecret;

  @Bean
  public Cloudinary cloudinary() {
    HashMap<Object, Object> config = Maps.newHashMap();
    config.put("cloud_name", this.cloudName);
    config.put("api_key", this.apiKey);
    config.put("api_secret", this.apiSecret);
    return new Cloudinary(config);
  }
}
