package de.home.cbl.jobbotapi.config;

import de.home.cbl.jobbotapi.auth.security.JwtTokenFilterConfigurer;
import de.home.cbl.jobbotapi.auth.security.JwtTokenProvider;
import de.home.cbl.jobbotapi.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired private JwtTokenProvider jwtTokenProvider;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // super.configure(http);
    http.csrf().disable();
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    // TODO @cbl remove job route from unauthenticated routes
    http.authorizeRequests()
        .antMatchers(
            Constants.ADMIN_MANAGE_API_PATH + "/**",
            Constants.ROUTE_AUTHENTICATIONS + "/**",
            Constants.ROUTE_JOBS + "/**",
            Constants.ROUTE_USERS + "/**")
        .permitAll()
        .anyRequest()
        .authenticated();

    // disable use of spring security default sign in page
    http.exceptionHandling().accessDeniedPage("/login");

    JwtTokenFilterConfigurer jwtTokenFilterConfigurer =
        new JwtTokenFilterConfigurer(this.jwtTokenProvider);
    http.apply(jwtTokenFilterConfigurer);

    // Optional, if you want to test the API from a browser
    http.httpBasic().disable();
  }

  @Override
  public void configure(WebSecurity web) {
    // Allow swagger to be accessed without authentication
    web.ignoring()
        .antMatchers("/v2/api-docs") //
        .antMatchers("/swagger-resources/**") //
        .antMatchers("/swagger-ui.html") //
        .antMatchers("/configuration/**") //
        .antMatchers("/webjars/**") //
        .antMatchers("/public")

        // Un-secure H2 Database (for testing purposes, H2 console shouldn't be unprotected in
        // production)
        // TODO @cbl disable for live
        .and()
        .ignoring()
        .antMatchers("/h2-console/**/**");
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
