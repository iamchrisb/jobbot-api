package de.home.cbl.jobbotapi.job.crawler;

import java.io.IOException;

public interface JobCrawlerInterface<T> {
  void crawlLatestJobs() throws IOException;
}
