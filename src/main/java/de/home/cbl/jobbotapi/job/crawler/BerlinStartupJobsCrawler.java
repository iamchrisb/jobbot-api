package de.home.cbl.jobbotapi.job.crawler;

import de.home.cbl.jobbotapi.job.model.db.JobEntity;
import de.home.cbl.jobbotapi.repository.JobEntityRepository;
import de.home.cbl.jobbotapi.utils.StringUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class BerlinStartupJobsCrawler implements JobCrawlerInterface<JobEntity> {

  public static final String PROVIDER_PLATFORM = "berlin-startup-jobs";
  public static final String BERLIN_STARTUP_JOBS_DATE_FORMAT = "MMMM dd, yyyy";
  private static final Logger log = LoggerFactory.getLogger(BerlinStartupJobsCrawler.class);
  private final int FIRST_INDEX = 0;
  private Connection connection;
  private DateTimeFormatter berlinStartupJobDateFormatter =
      DateTimeFormatter.ofPattern(BERLIN_STARTUP_JOBS_DATE_FORMAT, Locale.GERMAN);

  private String domain = "https://berlinstartupjobs.com/de/engineering/";
  private JobEntityRepository jobEntityRepository;

  public BerlinStartupJobsCrawler(@Autowired JobEntityRepository jobEntityRepository) {
    this.jobEntityRepository = jobEntityRepository;
    this.connection = Jsoup.connect(this.domain);
  }

  @Override
  public void crawlLatestJobs() throws IOException {
    List<JobEntity> allBerlinStartUpJobsInDatabase =
        this.jobEntityRepository.findAllByProviderPlatformOrderByCreatedDesc(PROVIDER_PLATFORM);

    Elements jobsOnWebsite = findAllJobsOnWebsite();
    String latestJobIdOnWebsite = jobsOnWebsite.first().attr("id");

    List<JobEntity> jobsAsEntities = getJobsAsEntitiesInCorrectOrder(jobsOnWebsite);

    if (allBerlinStartUpJobsInDatabase == null || allBerlinStartUpJobsInDatabase.isEmpty()) {
      this.jobEntityRepository.saveAll(jobsAsEntities);
      return;
    }

    JobEntity latestJobInDatabase = allBerlinStartUpJobsInDatabase.get(0);

    String latestJobIdInDatabase = latestJobInDatabase.getOriginalId();

    if (latestJobIdInDatabase.equals(latestJobIdOnWebsite)) {
      return;
    }

    List<JobEntity> onlyNewJobPosts = findLatestJobs(jobsAsEntities, latestJobIdInDatabase);
    this.jobEntityRepository.saveAll(onlyNewJobPosts);
  }

  private List<JobEntity> getJobsAsEntitiesInCorrectOrder(Elements jobsOnWebsite)
      throws IOException {
    // reverse order so we add and create the oldest jobs at first in the database
    Collections.reverse(jobsOnWebsite);

    List<JobEntity> jobs = new LinkedList<>();

    for (Element jobPostingElem : jobsOnWebsite) {
      JobEntity currentJob = getJobEntity(jobPostingElem);
      jobs.add(currentJob);
    }

    return jobs;
  }

  private Elements findAllJobsOnWebsite() throws IOException {
    Element primaryContainer = this.connection.get().body().getElementById("primary");
    return primaryContainer.getElementsByClass("product-listing-link-block");
  }

  private JobEntity getJobEntity(Element jobPostingElem) {
    JobEntity currentJob = new JobEntity();
    currentJob.setProviderPlatform(PROVIDER_PLATFORM);

    String externalJobId = jobPostingElem.attr("id");
    String link = StringUtil.subStringBetween(jobPostingElem.attr("onClick"), "'");

    currentJob.setId(UUID.randomUUID());
    currentJob.setCreated(LocalDateTime.now());

    currentJob.setLink(link);
    currentJob.setOriginalId(externalJobId);

    Element h2 = jobPostingElem.getElementsByTag("h2").first();
    if (h2 != null) {
      Element title = h2.getElementsByTag("a").first();
      if (title != null) {
        currentJob.setTitle(title.text());
      }
    }

    Element jobDate = jobPostingElem.getElementsByClass("product-listing-date").first();
    if (jobDate != null) {
      LocalDate datePosted = LocalDate.parse(jobDate.text(), this.berlinStartupJobDateFormatter);
      currentJob.setDatePosted(datePosted);
    }

    Element jobDescription = jobPostingElem.getElementsByClass("product-listing-p").first();
    if (jobDescription != null) {
      currentJob.setDescription(jobDescription.text());
    }
    return currentJob;
  }

  private List<JobEntity> findLatestJobs(List<JobEntity> jobs, String latestExternalId) {
    int splitIndex = 0;
    for (int i = 0; i < jobs.size(); i++) {
      if (jobs.get(i).getOriginalId().equals(latestExternalId)) {
        splitIndex = i;
        break;
      }
    }

    splitIndex += 1;

    List<JobEntity> berlinStartupJobsRealLatest = jobs.subList(splitIndex, jobs.size());
    return berlinStartupJobsRealLatest;
  }
}
