package de.home.cbl.jobbotapi.job.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JobCrawlerScheduler {

  // five minutes
  public static final long POLL_RATE_IN_MS = 1000 * 60 * 5;

  private static final Logger log = LoggerFactory.getLogger(JobCrawlerScheduler.class);
  private BerlinStartupJobsCrawler crawler;

  public JobCrawlerScheduler(@Autowired BerlinStartupJobsCrawler crawler) {
    this.crawler = crawler;
  }

  // TODO @cbl enable scheduler again
//  @Scheduled(fixedRate = POLL_RATE_IN_MS)
  public void scheduleCrawl() {
    try {
      this.crawler.crawlLatestJobs();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
