package de.home.cbl.jobbotapi.job;

import de.home.cbl.jobbotapi.job.model.JobConverter;
import de.home.cbl.jobbotapi.job.model.db.JobEntity;
import de.home.cbl.jobbotapi.job.model.dto.JobDTO;
import de.home.cbl.jobbotapi.repository.JobEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class JobService {
  private JobEntityRepository jobEntityRepository;
  private JobConverter converter;

  public JobService(
          @Autowired JobEntityRepository jobEntityRepository, @Autowired JobConverter converter) {
    this.jobEntityRepository = jobEntityRepository;
    this.converter = converter;
  }

  public JobDTO createJob(JobDTO job) {
    JobEntity jobEntity = this.converter.convertDto(job);
    jobEntity.setCreated(LocalDateTime.now());
    JobEntity savedJob = this.jobEntityRepository.save(jobEntity);
    return this.converter.convertEntity(savedJob);
  }

  public List<JobDTO> saveJobs(List<JobDTO> jobs) {

    return null;
  }

  public JobDTO findById(UUID id) {
    Optional<JobEntity> jobEntityOptional = this.jobEntityRepository.findById(id);
    if (!jobEntityOptional.isPresent()) {
      return null;
    }
    return this.converter.convertEntity(jobEntityOptional.get());
  }

  public void deleteById(UUID id) {
    this.jobEntityRepository.deleteById(id);
  }

  public boolean existsById(UUID id) {
    return this.jobEntityRepository.existsById(id);
  }

  public JobDTO update(JobDTO jobDTO, UUID id) {
    // TODO use ID
    JobEntity savedJob = this.jobEntityRepository.save(this.converter.convertDto(jobDTO));
    return this.converter.convertEntity(savedJob);
  }

  public JobDTO patchUpdate(JobDTO jobDTO, UUID id) {
    throw new IllegalArgumentException("Not supported operation");
  }

  public List<JobDTO> findAll(String directionStr) {
    Sort.Direction direction = Sort.Direction.fromString(directionStr);
    List<JobEntity> jobEntities =
        this.jobEntityRepository.findAll(Sort.by(direction, JobDTO.DATE_POSTED_FIELD));
    List<JobDTO> dtos = this.converter.convertEntities(jobEntities);
    return dtos;
  }

  public List<JobDTO> search(String title) {
    List<JobEntity> jobEntities =
        this.jobEntityRepository.findByTitleContainingOrderByDatePosted(title);
    List<JobDTO> dtos = this.converter.convertEntities(jobEntities);
    return dtos;
  }

  public List<JobDTO> search(String title, String platform) {
    List<JobEntity> jobEntities =
        this.jobEntityRepository.findByTitleContainingAndProviderPlatformOrderByDatePosted(
            title, platform);

    List<JobDTO> dtos = this.converter.convertEntities(jobEntities);
    return dtos;
  }
}
