package de.home.cbl.jobbotapi.job.model.dto;

import de.home.cbl.jobbotapi.info.Address;
import de.home.cbl.jobbotapi.info.Contact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDTO {
  public static final String DATE_POSTED_FIELD = "datePosted";

  @NotNull private UUID id;

  private String originalId;

  private String link;

  private LocalDateTime created;

  private LocalDate datePosted;
  private String description;

  @NotNull @NotEmpty private String title;

  private String providerPlatform;

  private Address companyAddress;

  private Contact companyContact;
}
