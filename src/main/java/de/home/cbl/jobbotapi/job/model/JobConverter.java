package de.home.cbl.jobbotapi.job.model;

import de.home.cbl.jobbotapi.job.model.db.JobEntity;
import de.home.cbl.jobbotapi.job.model.dto.JobDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class JobConverter {

  private ModelMapper modelMapper;

  public JobConverter(@Autowired ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public JobEntity convertDto(JobDTO jobDTO) {
    return this.modelMapper.map(jobDTO, JobEntity.class);
  }

  public JobDTO convertEntity(JobEntity jobEntity) {
    return this.modelMapper.map(jobEntity, JobDTO.class);
  }

  public List<JobDTO> convertEntities(List<JobEntity> userEntities) {
    List<JobDTO> dtos = userEntities.stream().map(this::convertEntity).collect(Collectors.toList());
    return dtos;
  }

  public List<JobEntity> convertDtos(List<JobDTO> jobDTOS) {
    List<JobEntity> jobEntities =
        jobDTOS.stream().map(this::convertDto).collect(Collectors.toList());
    return jobEntities;
  }
}
