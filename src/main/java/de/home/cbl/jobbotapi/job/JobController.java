package de.home.cbl.jobbotapi.job;

import de.home.cbl.jobbotapi.job.model.dto.JobDTO;
import de.home.cbl.jobbotapi.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = Constants.ROUTE_JOBS, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class JobController {
  private JobService jobService;

  public JobController(@Autowired JobService jobService) {
    this.jobService = jobService;
  }

  @GetMapping
  public ResponseEntity<List<JobDTO>> getJobs(
      @RequestParam(value = "sortDirection", defaultValue = "desc") String sortDirection) {

    // TODO switch to parameter validation
    if (!sortDirection.equals(Sort.Direction.DESC.toString())
        || sortDirection.equals(Sort.Direction.ASC.toString())) {
      ResponseEntity.badRequest().build();
    }

    List<JobDTO> dtos = this.jobService.findAll(sortDirection);
    return ResponseEntity.ok(dtos);
  }

  @GetMapping(value = "/search")
  public ResponseEntity<List<JobDTO>> searchJobs(@RequestParam("title") String title) {
    List<JobDTO> dtos = this.jobService.search(title);
    return ResponseEntity.ok(dtos);
  }

  @GetMapping(value = "/query")
  public ResponseEntity<List<JobDTO>> queryJobs(
      @RequestParam("title") String title,
      @RequestParam("providerPlatform") String providerPlatform) {
    List<JobDTO> dtos = this.jobService.search(title, providerPlatform);
    return ResponseEntity.ok(dtos);
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<JobDTO> findJob(@PathVariable("id") UUID id) {
    JobDTO jobDTO = this.jobService.findById(id);
    if (jobDTO == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(jobDTO);
  }

  @PostMapping
  public ResponseEntity<JobDTO> createJob(@RequestBody JobDTO jobDTO) {
    if (this.jobService.existsById(jobDTO.getId())) {
      return ResponseEntity.badRequest().build();
    }
    JobDTO createdJob = this.jobService.createJob(jobDTO);
    return ResponseEntity.ok(createdJob);
  }

  @PostMapping(value = "/bulk")
  public ResponseEntity<List<JobDTO>> createJobs(@RequestBody List<JobDTO> jobDTOs) {
    List<JobDTO> dtos = this.jobService.saveJobs(jobDTOs);
    return ResponseEntity.ok(dtos);
  }

  @PutMapping(value = "/{id}")
  public ResponseEntity<JobDTO> updateJob(@RequestBody JobDTO jobDTO, @PathVariable("id") UUID id) {
    if (!this.jobService.existsById(id)) {
      return ResponseEntity.notFound().build();
    }
    JobDTO updatedJob = this.jobService.update(jobDTO, id);
    return ResponseEntity.ok(updatedJob);
  }

  @PatchMapping(value = "/{id}")
  public ResponseEntity<JobDTO> partialUpdateJob(
          @RequestBody JobDTO jobDTO, @PathVariable("id") UUID id) {
    throw new IllegalArgumentException("Unsupported method invocation");
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<JobDTO> deleteJob(@PathVariable("id") UUID id) {
    if (!this.jobService.existsById(id)) {
      return ResponseEntity.notFound().build();
    }

    this.jobService.deleteById(id);
    return ResponseEntity.ok(null);
  }
}
