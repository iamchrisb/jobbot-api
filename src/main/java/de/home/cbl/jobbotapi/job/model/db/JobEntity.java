package de.home.cbl.jobbotapi.job.model.db;

import de.home.cbl.jobbotapi.info.Address;
import de.home.cbl.jobbotapi.info.Contact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobEntity {
  @Id private UUID id;

  @Indexed(unique = true)
  private String originalId;

  private String link;

  @Indexed private LocalDateTime created;

  @Indexed private LocalDate datePosted;
  private String description;

  @Indexed private String title;

  @Indexed private String providerPlatform;

  private Address companyAddress;

  private Contact companyContact;
}
