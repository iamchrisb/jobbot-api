package de.home.cbl.jobbotapi.auth.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
  ROLE_ADMIN,
  ROLE_SUPER_ADMIN,
  ROLE_APPLICATION,
  ROLE_USER;

  @Override
  public String getAuthority() {
    return name();
  }
}
