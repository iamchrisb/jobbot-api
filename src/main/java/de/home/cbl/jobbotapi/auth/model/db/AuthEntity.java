package de.home.cbl.jobbotapi.auth.model.db;

import de.home.cbl.jobbotapi.auth.model.Role;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthEntity {

  @Id private UUID id;

  @NotNull
  @NotEmpty
  @Indexed(unique = true)
  private String email;

  @Indexed private boolean emailVerified;

  @Indexed private UUID emailVerifyToken;

  @Indexed private LocalDateTime requiredEmailVerificationOn;

  private String encryptedPassword;

  @Indexed private UUID userId;

  private List<Role> roles;
}
