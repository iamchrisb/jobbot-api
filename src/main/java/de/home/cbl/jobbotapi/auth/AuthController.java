package de.home.cbl.jobbotapi.auth;

import com.google.common.net.HttpHeaders;
import de.home.cbl.jobbotapi.auth.model.dto.JwtTokenDTO;
import de.home.cbl.jobbotapi.auth.model.dto.SignUpDTO;
import de.home.cbl.jobbotapi.user.UserService;
import de.home.cbl.jobbotapi.utils.Constants;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(
    value = Constants.ROUTE_AUTHENTICATIONS,
    produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

  private UserService userService;

  public AuthController(@Autowired UserService userService) {
    this.userService = userService;
  }

  @PostMapping(value = Constants.SIGN_UP)
  public ResponseEntity<SignUpDTO> signUp(
          @RequestHeader("email") String email, @RequestHeader("password") String password) {
    if (this.userService.existsByEmail(email)) {
      return ResponseEntity.badRequest().build();
    }
    return ResponseEntity.ok(this.userService.signUp(email, password));
  }

  @PostMapping(value = Constants.SIGN_IN)
  public ResponseEntity<JwtTokenDTO> signIn(
          @RequestHeader("email") String email, @RequestHeader("password") String password) {
    JwtTokenDTO jwtTokenDTO = this.userService.signin(email, password);
    return ResponseEntity.ok(jwtTokenDTO);
  }

  @PostMapping(value = Constants.SIGN_OUT)
  public ResponseEntity<String> signOut(
      @ApiParam(hidden = true) @RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
    // this.userService.signOut(token);
    return ResponseEntity.ok(token);
  }

  @GetMapping(value = Constants.VERIFY_EMAIL)
  public ResponseEntity<String> verifyEmailAddress(@RequestParam("token") UUID token) {
    this.userService.verifyEmail(token);
    return ResponseEntity.ok("");
  }

  @GetMapping(value = Constants.REQUEST_EMAIL_VERIFICATION)
  public ResponseEntity<String> requestEmailVerification(@RequestParam("email") String email) {
    this.userService.requestEmailVerification(email);
    return ResponseEntity.ok("");
  }
}
