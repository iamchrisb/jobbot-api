package de.home.cbl.jobbotapi.auth.security;

import de.home.cbl.jobbotapi.auth.model.db.AuthEntity;
import de.home.cbl.jobbotapi.repository.AuthEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JobBotUserDetailsService implements UserDetailsService {

  private AuthEntityRepository authEntityRepository;

  public JobBotUserDetailsService(@Autowired AuthEntityRepository authEntityRepository) {
    this.authEntityRepository = authEntityRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    AuthEntity auth = this.authEntityRepository.findByEmail(username);

    if (auth == null) {
      throw new UsernameNotFoundException("User with email: " + username + "not found");
    }
    return org.springframework.security.core.userdetails.User //
        .withUsername(username) //
        .password(auth.getEncryptedPassword()) //
        .authorities(auth.getRoles()) //
        .accountExpired(false) //
        .accountLocked(false) //
        .credentialsExpired(false) //
        .disabled(false) //
        .build();
  }
}
