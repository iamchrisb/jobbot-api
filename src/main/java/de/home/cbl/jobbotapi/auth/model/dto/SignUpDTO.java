package de.home.cbl.jobbotapi.auth.model.dto;

import de.home.cbl.jobbotapi.user.model.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDTO {
  private UserDTO user;
  private JwtTokenDTO token;
}
