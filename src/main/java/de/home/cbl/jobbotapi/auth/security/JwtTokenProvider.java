package de.home.cbl.jobbotapi.auth.security;

import com.google.common.net.HttpHeaders;
import de.home.cbl.jobbotapi.auth.model.Role;
import de.home.cbl.jobbotapi.exception.AuthException;
import de.home.cbl.jobbotapi.utils.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {
  // valid for 10 days
  private static final long JWT_TOKEN_VALIDITY = 1000 * 60 * 24 * 10;
  private static final int BEARER_KEY_LENGTH = 7;
  private static final String ROLES_CLAIMS_KEY = "auth";

  // TODO @cbl switch to secure key and location
  // TODO @cbl use env variable token secret instead of hard coded secret
  private String secretKey = "test-secret";
  private JobBotUserDetailsService jobBotUserDetailsService;

  public JwtTokenProvider(@Autowired JobBotUserDetailsService jobBotUserDetailsService) {
    this.jobBotUserDetailsService = jobBotUserDetailsService;
  }

  @PostConstruct
  protected void init() {
    this.secretKey = Base64.getEncoder().encodeToString(this.secretKey.getBytes());
  }

  public String createToken(String email, List<Role> roles) {
    Claims claims = Jwts.claims().setSubject(email);

    List<SimpleGrantedAuthority> filteredRoles =
        roles.stream()
            .map(s -> new SimpleGrantedAuthority(s.getAuthority()))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

    claims.put(ROLES_CLAIMS_KEY, filteredRoles);

    Date now = new Date();
    Date expireDate = new Date(now.getTime() + JWT_TOKEN_VALIDITY);

    String generatedToken =
        Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(now)
            .setExpiration(expireDate)
            .signWith(SignatureAlgorithm.HS256, this.secretKey)
            .compact();
    return Constants.TOKEN_BEARER_PREFIX + generatedToken;
  }

  public Authentication getAuthentication(String token) {
    // we use the email instead of a username for authentication
    String email = getSubject(token);
    UserDetails userDetails = this.jobBotUserDetailsService.loadUserByUsername(email);
    Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
    return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
  }

  public String getSubject(String token) {
    return Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token).getBody().getSubject();
  }

  public String resolveTokenWithoutPrefix(HttpServletRequest req) {
    String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
    if (bearerToken != null && bearerToken.startsWith(Constants.TOKEN_BEARER_PREFIX)) {
      String substring = bearerToken.substring(BEARER_KEY_LENGTH);
      return substring;
    }
    return null;
  }

  public String resolveTokenWithPrefix(HttpServletRequest req) {
    String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
    if (bearerToken != null && bearerToken.startsWith(Constants.TOKEN_BEARER_PREFIX)) {
      return bearerToken;
    }
    return null;
  }

  public boolean validateToken(String token) {
    try {
      Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token);
      return true;
    } catch (JwtException | IllegalArgumentException e) {
      throw new AuthException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
