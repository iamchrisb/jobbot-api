package de.home.cbl.jobbotapi.auth.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JwtTokenFilterConfigurer
    extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
  Logger logger = LoggerFactory.getLogger(JwtTokenFilterConfigurer.class);
  private JwtTokenProvider jwtTokenProvider;

  public JwtTokenFilterConfigurer(JwtTokenProvider jwtTokenProvider) {
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @Override
  public void configure(HttpSecurity httpSecurity) throws Exception {
    this.logger.info("Configure Jwt token filter");
    JwtTokenFilter customFilter = new JwtTokenFilter(this.jwtTokenProvider);
    httpSecurity.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
  }
}
