package de.home.cbl.jobbotapi.auth.linkedin;

import de.home.cbl.jobbotapi.auth.model.dto.LinkedInAuthToken;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LinkedInAuthController {

  @PostMapping("/linkedin/auth")
  public ResponseEntity<LinkedInAuthToken> linkedInApplicationAuth(
      @RequestBody LinkedInAuthToken auth) {
    return ResponseEntity.ok(auth);
  }

  @GetMapping("linkedin/auth")
  public ResponseEntity<String> linkedInMemberShipAuth(
          @RequestParam("code") String code, @RequestParam("state") String state) {
    return ResponseEntity.ok("code:" + code + " ########## status: " + state);
  }
}
