package de.home.cbl.jobbotapi.auth.security;

import de.home.cbl.jobbotapi.exception.AuthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {

  Logger logger = LoggerFactory.getLogger(JwtTokenFilter.class);

  private JwtTokenProvider jwtTokenProvider;

  public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @Override
  protected void doFilterInternal(
          HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    this.logger.info("filter request for valid token");
    String tokenFromRequest = this.jwtTokenProvider.resolveTokenWithoutPrefix(request);
    /**
     * this.logger.info( "Current token: " + tokenFromRequest + " // for user with email: " +
     * this.jwtTokenProvider.getSubject(tokenFromRequest));
     */
    try {
      if (tokenFromRequest != null && this.jwtTokenProvider.validateToken(tokenFromRequest)) {
        Authentication auth = this.jwtTokenProvider.getAuthentication(tokenFromRequest);
        SecurityContextHolder.getContext().setAuthentication(auth);
      }
    } catch (AuthException ex) {
      // this is very important, since it guarantees the user is not authenticated at all
      SecurityContextHolder.clearContext();
      response.sendError(ex.getHttpStatus().value(), ex.getMessage());
      return;
    }

    filterChain.doFilter(request, response);
  }
}
