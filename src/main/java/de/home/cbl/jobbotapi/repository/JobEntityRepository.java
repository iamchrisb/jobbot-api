package de.home.cbl.jobbotapi.repository;

import de.home.cbl.jobbotapi.job.model.db.JobEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface JobEntityRepository extends MongoRepository<JobEntity, UUID> {

  List<JobEntity> findByTitleContainingOrderByDatePosted(String title);

  List<JobEntity> findByTitleContainingAndProviderPlatformOrderByDatePosted(
      String title, String providerPlatform);

  List<JobEntity> findAllByProviderPlatform(String providerPlatform);

  List<JobEntity> findAllByProviderPlatformOrderByDatePostedDesc(String providerPlatform);

  List<JobEntity> findAllByProviderPlatformOrderByCreatedDesc(String providerPlatform);
}
