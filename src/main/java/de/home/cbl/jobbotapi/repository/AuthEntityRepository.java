package de.home.cbl.jobbotapi.repository;

import de.home.cbl.jobbotapi.auth.model.db.AuthEntity;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuthEntityRepository extends MongoRepository<AuthEntity, UUID> {
  void deleteByUserId(UUID userId);

  boolean existsByEmail(String email);

  AuthEntity findByEmail(String email);

  Optional<AuthEntity> findByUserId(UUID userId);

  Optional<AuthEntity> findByEmailVerifyToken(UUID token);
}
