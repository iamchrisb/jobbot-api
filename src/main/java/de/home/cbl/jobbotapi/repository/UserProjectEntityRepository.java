package de.home.cbl.jobbotapi.repository;

import de.home.cbl.jobbotapi.user.model.db.UserProjectEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserProjectEntityRepository extends MongoRepository<UserProjectEntity, UUID> {
  Optional<List<UserProjectEntity>> findByOwnerId(UUID ownerId);

  List<UserProjectEntity> findAllByOwnerId(UUID ownerId);
}
