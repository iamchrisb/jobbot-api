package de.home.cbl.jobbotapi.repository;

import de.home.cbl.jobbotapi.user.model.db.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface UserEntityRepository extends MongoRepository<UserEntity, UUID> {}
