package de.home.cbl.jobbotapi.utils;

public class CloudinaryConstants {
  public static final String URL_KEY = "url";
  public static final String FOLDER_KEY = "folder";
}
