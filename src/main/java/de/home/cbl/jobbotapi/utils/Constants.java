package de.home.cbl.jobbotapi.utils;

public class Constants {
  public static final Long DAYS_EMAIL_VERIFY_TOKEN_IS_VALID = 30l;

  public static final String API_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String API_DATE_FORMAT = "yyyy-MM-dd";

  public static final String TOKEN_BEARER_PREFIX = "Bearer ";

  public static final String API_VERSION = "/v0.1";
  public static final String API_PATH = "/api" + API_VERSION;
  public static final String ADMIN_API_PATH = "/api" + API_VERSION + "/admin";
  public static final String ADMIN_MANAGE_API_PATH = ADMIN_API_PATH + "/manage";

  public static final String ROUTE_USERS = API_PATH + "/users";
  public static final String ROUTE_JOBS = API_PATH + "/jobs";
  public static final String ROUTE_AUTHENTICATIONS = API_PATH + "/authentications";

  public static final String SIGN_OUT = "/signout";
  public static final String SIGN_IN = "/signin";
  public static final String SIGN_UP = "/signup";
  public static final String VERIFY_EMAIL = "/verify-email";
  public static final String REQUEST_EMAIL_VERIFICATION = "/request-email-verification";

  public static final String UPLOAD_IMAGE = "/upload";

  public static final String SIGN_UP_PATH = API_PATH + SIGN_UP;
  public static final String SIGN_IN_PATH = API_PATH + SIGN_IN;
  public static final String VERIFY_EMAIL_PATH = API_PATH + VERIFY_EMAIL;
  public static final String REQUEST_EMAIL_VERIFICATION_PATH =
      API_PATH + REQUEST_EMAIL_VERIFICATION;

  public static final String SEARCH = "/search";
  public static final String TERMS_OF_SERVICE_URL = "https://termsofserviceurl.com";
  public static final String API_DESCRIPTION =
      "An api for the twire app that delivers information about cool jobs from the startup scene in the tech branch and makes it possible to manage a users documents and data like the CV, projects etc.";
  public static final String API_TITLE = "twire-App RESTful API documentation";
  public static final String API_LICENSE = "Apache 2.0";
  public static final String API_LICENSE_URL = "http://www.apache.org/licenses/LICENSE-2.0";
  public static final String CONTACT_PERSON = "Christopher Bleckmann";
  public static final String CONTACT_URL = "https://iamchrisb.de";
  public static final String CONTACT_EMAIL = "mail@iamchrisb.de";
}
