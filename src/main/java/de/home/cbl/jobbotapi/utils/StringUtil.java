package de.home.cbl.jobbotapi.utils;

public class StringUtil {
  public static String subStringBetween(String value, String char1, String char2) {
    if (value == null || value.isEmpty()) {
      throw new IllegalArgumentException("value can not be null!");
    }

    if (char1 == null || char1.isEmpty()) {
      throw new IllegalArgumentException("value can not be null!");
    }

    if (char2 == null || char2.isEmpty()) {
      throw new IllegalArgumentException("value can not be null!");
    }
    int index1 = value.indexOf(char1) + 1;
    String subString = value.substring(index1, value.length());
    int index2 = -1 + index1 + subString.indexOf(char2);
    return value.substring(index1, index2);
  }

  public static String subStringBetween(String value, String char1) {
    return subStringBetween(value, char1, char1);
  }
}
