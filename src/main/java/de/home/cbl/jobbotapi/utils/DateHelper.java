package de.home.cbl.jobbotapi.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateHelper {
  public static DateTimeFormatter API_FORMATTER =
      DateTimeFormatter.ofPattern(Constants.API_DATE_TIME_FORMAT);

  public static LocalDate formatLocalDate(LocalDate date, String fromPattern, String toPattern) {
    return null;
  }

  public static LocalDate formatLocalDateToApiFormat(LocalDate date, String oldFormat) {
    return null;
  }

  public static LocalDateTime formatLocalDateTime(
          LocalDateTime localDateTime, String formatFrom, String formatTo) {
    return null;
  }

  public static LocalDateTime formatLocalDateTimeToApiFormat(
          LocalDateTime localDateTime, String formatFrom) {
    return null;
  }

  //  public static LocalDateTime getNowInApiFormat() {
  //    LocalDateTime currentLocalDateTime = LocalDateTime.now();
  //    currentLocalDateTime.format(API_FORMATTER);
  //    return currentLocalDateTime;
  //  }
}
