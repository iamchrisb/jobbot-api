package de.home.cbl.jobbotapi.exception.messages;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiErrorMessage {
  private String errorCode;
  private HttpStatus httpStatus;
  private String message;
}
