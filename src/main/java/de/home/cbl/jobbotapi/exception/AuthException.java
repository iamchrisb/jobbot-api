package de.home.cbl.jobbotapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public class AuthException extends RuntimeException {

  private final String message;
  private final HttpStatus httpStatus;
}
