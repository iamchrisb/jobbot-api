package de.home.cbl.jobbotapi.exception;

import de.home.cbl.jobbotapi.exception.messages.ApiErrorMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TwireException extends RuntimeException {
  private ApiErrorMessage exceptionMessage;
}
