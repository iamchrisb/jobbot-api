package de.home.cbl.jobbotapi.exception.messages;

import org.springframework.http.HttpStatus;

public class ApiErrorMessages {

  public static final ApiErrorMessage INVALID_EMAIL_VERIFY_TOKEN =
      ApiErrorMessage.builder()
          .message("Invalid email token")
          .httpStatus(HttpStatus.BAD_REQUEST)
          .build();
  public static final ApiErrorMessage EMAIL_VERIFY_TOKEN_EXPIRED =
      ApiErrorMessage.builder()
          .message("Email verification token expired")
          .httpStatus(HttpStatus.BAD_REQUEST)
          .build();
  public static final ApiErrorMessage ACCOUNT_NOT_FOUND =
      ApiErrorMessage.builder()
          .httpStatus(HttpStatus.NOT_FOUND)
          .message("Account does not exist")
          .build();
  public static final ApiErrorMessage WRONG_CREDENTIALS_SUBMITTED =
      ApiErrorMessage.builder()
          .message("Invalid credentials submitted")
          .httpStatus(HttpStatus.BAD_REQUEST)
          .build();
  public static final ApiErrorMessage NO_PROJECTS_FOUND =
      ApiErrorMessage.builder().httpStatus(HttpStatus.NOT_FOUND).build();
  public static final ApiErrorMessage PROJECT_NOT_FOUND =
      ApiErrorMessage.builder().httpStatus(HttpStatus.NOT_FOUND).build();
  public static final ApiErrorMessage NOT_SUPPORTED_METHOD =
      ApiErrorMessage.builder()
          .httpStatus(HttpStatus.BAD_REQUEST)
          .message("Method is not supported")
          .build();
  public static final ApiErrorMessage UPLOADING_IMAGE_FAILED =
      ApiErrorMessage.builder()
          .message("Image upload failed")
          .httpStatus(HttpStatus.BAD_REQUEST)
          .build();
}
