package de.home.cbl.jobbotapi.admin;

import com.cloudinary.Cloudinary;
import com.google.common.collect.Maps;
import de.home.cbl.jobbotapi.exception.TwireException;
import de.home.cbl.jobbotapi.exception.messages.ApiErrorMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ImageService {
  public static final String DEFAULT_PROJECT_IMG_URL =
      "https://res.cloudinary.com/di5m94equ/image/upload/v1570443212/samples/bike.jpg";
  private Cloudinary cloudinary;

  public ImageService(@Autowired Cloudinary cloudinary) {
    this.cloudinary = cloudinary;
  }

  public Map uploadMultipartFile(MultipartFile multipartFile, Map<Object, Object> uploadConfig) {
    Map uploadResult = null;
    try {
      uploadResult = this.cloudinary.uploader().upload(multipartFile.getBytes(), uploadConfig);
    } catch (IOException e) {
      throw new TwireException(ApiErrorMessages.UPLOADING_IMAGE_FAILED);
    }
    return uploadResult;
  }

  public Map uploadMultipartFile(MultipartFile multipartFile) {
    HashMap<Object, Object> emptyConfig = Maps.newHashMap();
    return uploadMultipartFile(multipartFile, emptyConfig);
  }

  public Map uploadFile(File file) {
    HashMap<Object, Object> uploadConfigs = Maps.newHashMap();
    Map uploadResult = null;
    try {
      uploadResult = this.cloudinary.uploader().upload(file, uploadConfigs);
    } catch (IOException e) {
      throw new TwireException(ApiErrorMessages.UPLOADING_IMAGE_FAILED);
    }
    return uploadResult;
  }
}
