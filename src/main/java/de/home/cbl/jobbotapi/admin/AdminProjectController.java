package de.home.cbl.jobbotapi.admin;

import de.home.cbl.jobbotapi.user.model.dto.UserProjectDTO;
import de.home.cbl.jobbotapi.user.projects.ProjectService;
import de.home.cbl.jobbotapi.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(
    value = Constants.ADMIN_MANAGE_API_PATH + "/projects",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminProjectController {

  private ProjectService projectService;

  public AdminProjectController(@Autowired ProjectService projectService) {
    this.projectService = projectService;
  }

  @GetMapping
  public ResponseEntity<List<UserProjectDTO>> getProjects(
      @RequestParam(value = "ownerId", required = false) UUID ownerId) {
    List<UserProjectDTO> projects = new LinkedList<>();
    if (ownerId == null) {
      projects = this.projectService.getProjects();
    } else {
      projects = this.projectService.findProjectsByOwnerId(ownerId);
    }
    return ResponseEntity.ok(projects);
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<UserProjectDTO> getProject(@PathVariable("id") UUID id) {
    UserProjectDTO project = this.projectService.findById(id);
    return ResponseEntity.ok(project);
  }
}
