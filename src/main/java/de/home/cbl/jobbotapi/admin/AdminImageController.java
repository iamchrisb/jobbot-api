package de.home.cbl.jobbotapi.admin;

import de.home.cbl.jobbotapi.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

@RestController
@RequestMapping(
    value = Constants.ADMIN_MANAGE_API_PATH + "/images",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminImageController {

  private ImageService imageService;

  public AdminImageController(@Autowired ImageService imageService) {
    this.imageService = imageService;
  }

  @PostMapping(value = Constants.UPLOAD_IMAGE)
  public ResponseEntity<Map<Object, Object>> uploadImage(
          @RequestParam("file") MultipartFile multipartFile, RedirectAttributes redirectAttributes) {
    Map map = this.imageService.uploadMultipartFile(multipartFile);
    return ResponseEntity.ok(map);
  }
}
