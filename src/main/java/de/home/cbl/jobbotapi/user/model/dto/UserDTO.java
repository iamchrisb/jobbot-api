package de.home.cbl.jobbotapi.user.model.dto;

import de.home.cbl.jobbotapi.info.Address;
import de.home.cbl.jobbotapi.info.Contact;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO {

  @NotNull private UUID id;

  private String email;

  private String displayName;

  private String telegramChatId;

  private LocalDateTime dateLastCrawled;

  @NotNull private LocalDateTime created;

  private String profileImageUrl;

  private Contact contact;

  private Address address;
}
