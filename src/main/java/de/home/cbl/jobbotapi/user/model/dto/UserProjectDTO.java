package de.home.cbl.jobbotapi.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserProjectDTO {

  @NotNull private UUID id;

  private UUID ownerId;

  @NotNull @NotEmpty private String title;

  private LocalDateTime created;

  private LocalDateTime changed;

  private String shortDescription;

  private String description;

  private String coverImgUrl;
}
