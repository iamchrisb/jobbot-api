package de.home.cbl.jobbotapi.user.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserProjectEntity {

  @Id private UUID id;

  @Indexed private UUID ownerId;

  @Indexed private String title;

  @Indexed private LocalDateTime created;

  @Indexed private LocalDateTime changed;

  private String shortDescription;

  private String description;

  private String coverImgUrl;
}
