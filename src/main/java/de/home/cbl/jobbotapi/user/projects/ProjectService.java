package de.home.cbl.jobbotapi.user.projects;

import de.home.cbl.jobbotapi.admin.ImageService;
import de.home.cbl.jobbotapi.exception.TwireException;
import de.home.cbl.jobbotapi.exception.messages.ApiErrorMessages;
import de.home.cbl.jobbotapi.repository.UserProjectEntityRepository;
import de.home.cbl.jobbotapi.user.model.ProjectConverter;
import de.home.cbl.jobbotapi.user.model.db.UserProjectEntity;
import de.home.cbl.jobbotapi.user.model.dto.UserProjectDTO;
import de.home.cbl.jobbotapi.utils.CloudinaryConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectService {

  private UserProjectEntityRepository userProjectEntityRepository;
  private ProjectConverter projectConverter;

  public ProjectService(
      @Autowired UserProjectEntityRepository userProjectEntityRepository,
      @Autowired ProjectConverter projectConverter) {
    this.userProjectEntityRepository = userProjectEntityRepository;
    this.projectConverter = projectConverter;
  }

  public List<UserProjectDTO> findProjectsByOwnerId(UUID ownerId) {
    List<UserProjectEntity> projectEntities =
        this.userProjectEntityRepository.findAllByOwnerId(ownerId);

    if (projectEntities == null || projectEntities.isEmpty()) {
      throw new TwireException(ApiErrorMessages.NO_PROJECTS_FOUND);
    }
    return this.projectConverter.convertEntities(projectEntities);
  }

  public UserProjectDTO create(UserProjectDTO projectDto, UUID userId) {
    if (projectDto.getId() == null) {
      projectDto.setId(UUID.randomUUID());
    }

    if (projectDto.getCoverImgUrl() == null || projectDto.getCoverImgUrl().isEmpty()) {
      projectDto.setCoverImgUrl(ImageService.DEFAULT_PROJECT_IMG_URL);
    }

    projectDto.setOwnerId(userId);

    UserProjectEntity userProjectEntity = this.projectConverter.convertDto(projectDto);
    this.userProjectEntityRepository.save(userProjectEntity);
    return null;
  }

  public UserProjectDTO delete(UUID projectId) {
    UserProjectDTO projectDTO = findById(projectId);
    this.userProjectEntityRepository.deleteById(projectId);
    return projectDTO;
  }

  public boolean exists(UUID projectId) {
    return this.userProjectEntityRepository.existsById(projectId);
  }

  public UserProjectDTO findById(UUID projectId) {

    Optional<UserProjectEntity> optional = this.userProjectEntityRepository.findById(projectId);
    if (!optional.isPresent()) {
      throw new TwireException(ApiErrorMessages.PROJECT_NOT_FOUND);
    }

    return this.projectConverter.convertEntity(optional.get());
  }

  public UserProjectDTO changePreviewImage(Map map, UUID projectId) {
    Optional<UserProjectEntity> projectEntityOptional =
        this.userProjectEntityRepository.findById(projectId);
    String coverImgUrl = (String) map.get(CloudinaryConstants.URL_KEY);
    UserProjectEntity userProjectEntity = projectEntityOptional.get();
    userProjectEntity.setCoverImgUrl(coverImgUrl);
    UserProjectEntity updatedProject = this.userProjectEntityRepository.save(userProjectEntity);
    return this.projectConverter.convertEntity(userProjectEntity);
  }

  public List<UserProjectDTO> getProjects() {
    List<UserProjectEntity> all = this.userProjectEntityRepository.findAll();
    return this.projectConverter.convertEntities(all);
  }
}
