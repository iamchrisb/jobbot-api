package de.home.cbl.jobbotapi.user.model.db;

import de.home.cbl.jobbotapi.info.Address;
import de.home.cbl.jobbotapi.info.Contact;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserEntity {

  @Id private UUID id;

  @Indexed private String displayName;

  private String telegramChatId;

  @Indexed private LocalDateTime dateLastCrawled;

  @Indexed private LocalDateTime created;

  @Indexed private LocalDateTime changed;

  private String profileImageUrl;

  private Contact contact;

  private Address address;
}
