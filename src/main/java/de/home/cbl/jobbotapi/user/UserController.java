package de.home.cbl.jobbotapi.user;

import com.google.common.collect.Maps;
import de.home.cbl.jobbotapi.admin.ImageService;
import de.home.cbl.jobbotapi.exception.TwireException;
import de.home.cbl.jobbotapi.exception.messages.ApiErrorMessages;
import de.home.cbl.jobbotapi.user.model.dto.UserDTO;
import de.home.cbl.jobbotapi.user.model.dto.UserProjectDTO;
import de.home.cbl.jobbotapi.user.projects.ProjectService;
import de.home.cbl.jobbotapi.utils.CloudinaryConstants;
import de.home.cbl.jobbotapi.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = Constants.ROUTE_USERS, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

  private UserService userService;
  private ProjectService projectService;
  private ImageService imageService;

  public UserController(
      @Autowired UserService userService,
      @Autowired ProjectService projectService,
      @Autowired ImageService imageService) {
    this.userService = userService;
    this.projectService = projectService;
    this.imageService = imageService;
  }

  @GetMapping
  public ResponseEntity<List<UserDTO>> getUsers() {
    List<UserDTO> userDTOS = this.userService.findAll();
    return ResponseEntity.ok(userDTOS);
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<UserDTO> getUser(@PathVariable("id") UUID id) {
    UserDTO userDTO = this.userService.findById(id);
    return ResponseEntity.ok(userDTO);
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<UserDTO> deleteUser(@PathVariable("id") UUID id) {
    UserDTO userDTO = this.userService.findById(id);
    if (userDTO == null) {
      return ResponseEntity.notFound().build();
    }

    this.userService.deleteById(id);
    return ResponseEntity.ok(userDTO);
  }

  @PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<UserDTO> partialUpdateUser(
          @RequestBody UserDTO userToUpdate, @PathVariable("id") UUID id) {
    if (!this.userService.exists(id)) {
      return ResponseEntity.notFound().build();
    }

    UserDTO updatedUser = this.userService.updatePartial(userToUpdate, id);
    return ResponseEntity.ok(updatedUser);
  }

  @PostMapping(value = "/{userId}/images/profile")
  public ResponseEntity<UserDTO> uploadProjectImage(
          @PathVariable("userId") UUID userId, @RequestParam("file") MultipartFile multipartFile) {
    if (!this.userService.exists(userId)) {
      return ResponseEntity.notFound().build();
    }

    HashMap<Object, Object> uploadConfig = Maps.newHashMap();
    uploadConfig.put(CloudinaryConstants.FOLDER_KEY, getUserImageFolder(userId));

    Map map = this.imageService.uploadMultipartFile(multipartFile, uploadConfig);
    String profileImageUrl = (String) map.get(CloudinaryConstants.URL_KEY);

    UserDTO userDTO = this.userService.changeProfileImage(profileImageUrl, userId);
    return ResponseEntity.ok(userDTO);
  }

  private String getUserImageFolder(UUID userId) {
    return String.format("/users/%s/images", userId);
  }

  /**
   * projects
   *
   * <p>*
   */
  @GetMapping(value = "/{userId}/projects")
  public ResponseEntity<List<UserProjectDTO>> getUserProjects(@PathVariable("userId") UUID userId) {
    if (!this.userService.exists(userId)) {
      return ResponseEntity.notFound().build();
    }
    List<UserProjectDTO> projects = this.projectService.findProjectsByOwnerId(userId);
    return ResponseEntity.ok(projects);
  }

  @PostMapping(value = "/{userId}/projects")
  public ResponseEntity<UserProjectDTO> createProject(
          @RequestBody UserProjectDTO projectDto, @PathVariable("userId") UUID userId) {
    if (this.userService.exists(userId)) {
      return ResponseEntity.notFound().build();
    }
    UserProjectDTO project = this.projectService.create(projectDto, userId);
    return ResponseEntity.ok(project);
  }

  @GetMapping(value = "/{userId}/projects/{projectId}")
  public ResponseEntity<UserProjectDTO> getProject(
          @PathVariable("userId") UUID userId, @PathVariable("projectId") UUID projectId) {
    return ResponseEntity.ok(this.projectService.findById(projectId));
  }

  @PostMapping(value = "/{userId}/projects/{projectId}/images/cover")
  public ResponseEntity<UserProjectDTO> uploadProjectImage(
          @PathVariable("userId") UUID userId,
          @PathVariable("projectId") UUID projectId,
          @RequestParam("file") MultipartFile multipartFile,
          RedirectAttributes redirectAttributes) {
    if (!this.userService.exists(userId)) {
      return ResponseEntity.notFound().build();
    }

    if (!this.projectService.exists(projectId)) {
      return ResponseEntity.notFound().build();
    }

    HashMap<Object, Object> uploadConfig = Maps.newHashMap();
    uploadConfig.put(CloudinaryConstants.FOLDER_KEY, getProjectImageFolderPath(userId, projectId));
    Map map = this.imageService.uploadMultipartFile(multipartFile, uploadConfig);

    UserProjectDTO projectDTO = this.projectService.changePreviewImage(map, projectId);
    return ResponseEntity.ok(projectDTO);
  }

  @PutMapping(value = "/{userId}/projects/{projectId}")
  public ResponseEntity<UserProjectDTO> updateProject() {
    throw new TwireException(ApiErrorMessages.NOT_SUPPORTED_METHOD);
  }

  @DeleteMapping(value = "/{userId}/projects/{projectId}")
  public ResponseEntity<UserProjectDTO> deleteProject(
          @PathVariable("userId") UUID userId, @PathVariable("projectId") UUID projectId) {
    if (this.userService.exists(userId)) {
      return ResponseEntity.notFound().build();
    }

    UserProjectDTO projectDTO = this.projectService.delete(projectId);
    return ResponseEntity.ok(projectDTO);
  }

  private String getProjectImageFolderPath(UUID userId, UUID projectId) {
    return String.format("users/%s/images/projects/%s", userId, projectId);
  }
}
