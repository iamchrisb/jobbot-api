package de.home.cbl.jobbotapi.user.model;

import de.home.cbl.jobbotapi.user.model.db.UserProjectEntity;
import de.home.cbl.jobbotapi.user.model.dto.UserProjectDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProjectConverter {

  private ModelMapper modelMapper;

  public ProjectConverter(@Autowired ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public UserProjectEntity convertDto(UserProjectDTO projectDTO) {
    return this.modelMapper.map(projectDTO, UserProjectEntity.class);
  }

  public UserProjectDTO convertEntity(UserProjectEntity projectEntity) {
    return this.modelMapper.map(projectEntity, UserProjectDTO.class);
  }

  public List<UserProjectDTO> convertEntities(List<UserProjectEntity> projectEntities) {
    List<UserProjectDTO> projectDTOS =
        projectEntities.stream().map(this::convertEntity).collect(Collectors.toList());
    return projectDTOS;
  }
}
