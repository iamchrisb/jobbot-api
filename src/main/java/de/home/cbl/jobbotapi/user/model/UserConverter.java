package de.home.cbl.jobbotapi.user.model;

import de.home.cbl.jobbotapi.user.model.db.UserEntity;
import de.home.cbl.jobbotapi.user.model.dto.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {

  private ModelMapper modelMapper;

  public UserConverter(@Autowired ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public UserEntity convertDto(UserDTO userDTO) {
    return this.modelMapper.map(userDTO, UserEntity.class);
  }

  public UserDTO convertEntity(UserEntity userEntity) {
    return this.modelMapper.map(userEntity, UserDTO.class);
  }

  public List<UserDTO> convertEntities(List<UserEntity> userEntities) {
    List<UserDTO> userDTOS =
        userEntities.stream().map(this::convertEntity).collect(Collectors.toList());
    return userDTOS;
  }
}
