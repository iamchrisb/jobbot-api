package de.home.cbl.jobbotapi.user;

import de.home.cbl.jobbotapi.auth.model.Role;
import de.home.cbl.jobbotapi.auth.model.db.AuthEntity;
import de.home.cbl.jobbotapi.auth.model.dto.JwtTokenDTO;
import de.home.cbl.jobbotapi.auth.model.dto.SignUpDTO;
import de.home.cbl.jobbotapi.auth.security.JwtTokenProvider;
import de.home.cbl.jobbotapi.email.EmailService;
import de.home.cbl.jobbotapi.exception.TwireException;
import de.home.cbl.jobbotapi.exception.messages.ApiErrorMessages;
import de.home.cbl.jobbotapi.repository.AuthEntityRepository;
import de.home.cbl.jobbotapi.repository.UserEntityRepository;
import de.home.cbl.jobbotapi.user.model.UserConverter;
import de.home.cbl.jobbotapi.user.model.db.UserEntity;
import de.home.cbl.jobbotapi.user.model.dto.UserDTO;
import de.home.cbl.jobbotapi.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.DAYS;

@Component
public class UserService {

  Logger logger = LoggerFactory.getLogger(UserService.class);

  private UserEntityRepository userEntityRepository;
  private UserConverter userConverter;
  private AuthEntityRepository authEntityRepository;
  private PasswordEncoder passwordEncoder;
  private JwtTokenProvider jwtTokenProvider;
  private AuthenticationManager authenticationManager;
  private EmailService emailService;

  public UserService(
      @Autowired UserEntityRepository userEntityRepository,
      @Autowired UserConverter userConverter,
      @Autowired AuthEntityRepository authEntityRepository,
      @Autowired PasswordEncoder passwordEncoder,
      @Autowired JwtTokenProvider jwtTokenProvider,
      @Autowired AuthenticationManager authenticationManager,
      @Autowired EmailService emailService) {
    this.userEntityRepository = userEntityRepository;
    this.userConverter = userConverter;
    this.authEntityRepository = authEntityRepository;
    this.passwordEncoder = passwordEncoder;
    this.jwtTokenProvider = jwtTokenProvider;
    this.authenticationManager = authenticationManager;
    this.emailService = emailService;
  }

  public List<UserDTO> findAll() {
    List<UserEntity> all = this.userEntityRepository.findAll();
    return this.userConverter.convertEntities(all);
  }

  public void deleteById(UUID id) {
    this.authEntityRepository.deleteByUserId(id);
    this.userEntityRepository.deleteById(id);

    if (this.authEntityRepository.findByUserId(id).isPresent()) {
      throw new RuntimeException("User could not be deleted correctly");
    }

    if (this.userEntityRepository.findById(id).isPresent()) {
      throw new RuntimeException("User could not be deleted correctly");
    }
  }

  public UserDTO findById(UUID id) {
    Optional<UserEntity> userEntityOptional = this.userEntityRepository.findById(id);
    if (!userEntityOptional.isPresent()) {
      throw new IllegalArgumentException("user not found");
    }
    return this.userConverter.convertEntity(userEntityOptional.get());
  }

  public boolean exists(UUID id) {
    return this.userEntityRepository.existsById(id);
  }

  public UserDTO updatePartial(UserDTO userToUpdate, UUID id) {
    throw new RuntimeException("not supported method");
  }

  public boolean existsByEmail(String email) {
    return this.authEntityRepository.findByEmail(email) != null;
  }

  public SignUpDTO signUp(String email, String password) {
    UUID userId = UUID.randomUUID();
    String hashedPassword = this.passwordEncoder.encode(password);

    LinkedList<Role> roles = new LinkedList<>();
    roles.add(Role.ROLE_USER);

    UUID verificationToken = UUID.randomUUID();

    AuthEntity authToSave =
        AuthEntity.builder()
            .id(UUID.randomUUID())
            .email(email)
            .requiredEmailVerificationOn(LocalDateTime.now())
            .emailVerifyToken(verificationToken)
            .encryptedPassword(hashedPassword)
            .userId(userId)
            .roles(roles)
            .build();

    UserEntity userToSave = UserEntity.builder().id(userId).created(LocalDateTime.now()).build();
    UserEntity savedUserEntity = this.userEntityRepository.save(userToSave);

    this.authEntityRepository.save(authToSave);

    UserDTO userDTO = this.userConverter.convertEntity(savedUserEntity);
    userDTO.setEmail(authToSave.getEmail());

    String token = this.jwtTokenProvider.createToken(email, roles);

    SignUpDTO signUpDTO =
        SignUpDTO.builder()
            .token(JwtTokenDTO.builder().bearerToken(token).build())
            .user(userDTO)
            .build();

    try {
      this.emailService.sendRequestEmailVerification(email, verificationToken);
    } catch (Exception e) {
      this.logger.warn("email could not be sent! " + e.getMessage());
    }

    return signUpDTO;
  }

  public boolean isAuthenticated(String email, String password) {
    AuthEntity authEntity = this.authEntityRepository.findByEmail(email);
    return this.passwordEncoder.matches(password, authEntity.getEncryptedPassword());
  }

  public JwtTokenDTO signin(String email, String password) {
    try {
      Authentication authentication =
          this.authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(email, password));

      String token =
          this.jwtTokenProvider.createToken(
              email, this.authEntityRepository.findByEmail(email).getRoles());

      return JwtTokenDTO.builder().bearerToken(token).build();
    } catch (AuthenticationException e) {
      throw new TwireException(ApiErrorMessages.WRONG_CREDENTIALS_SUBMITTED);
    }
  }

  public UserDTO findByEmail(String email) {
    AuthEntity authEntity = this.authEntityRepository.findByEmail(email);
    Optional<UserEntity> userEntity = this.userEntityRepository.findById(authEntity.getUserId());
    if (!userEntity.isPresent()) {
      throw new IllegalArgumentException("user not found");
    }
    UserDTO userDTO = this.userConverter.convertEntity(userEntity.get());
    userDTO.setEmail(authEntity.getEmail());
    return userDTO;
  }

  public String signOut(String token) {
    // TODO @cbl revoke JWT token
    return null;
  }

  public void verifyEmail(UUID emailVerifyToken) {
    Optional<AuthEntity> tokenOptional =
        this.authEntityRepository.findByEmailVerifyToken(emailVerifyToken);

    if (!tokenOptional.isPresent()) {
      throw new TwireException(ApiErrorMessages.INVALID_EMAIL_VERIFY_TOKEN);
    }

    AuthEntity authEntity = tokenOptional.get();

    LocalDateTime today = LocalDateTime.now();
    long daysBetween = DAYS.between(authEntity.getRequiredEmailVerificationOn(), today);

    if (daysBetween > Constants.DAYS_EMAIL_VERIFY_TOKEN_IS_VALID) {
      throw new TwireException((ApiErrorMessages.EMAIL_VERIFY_TOKEN_EXPIRED));
    }

    authEntity.setEmailVerified(true);
    authEntity.setEmailVerifyToken(null);
    this.authEntityRepository.save(authEntity);

    this.emailService.sendEmailVerificationSucceed(authEntity.getEmail());
  }

  public void requestEmailVerification(String email) {
    if (!this.authEntityRepository.existsByEmail(email)) {
      throw new TwireException(ApiErrorMessages.ACCOUNT_NOT_FOUND);
    }

    AuthEntity authEntity = this.authEntityRepository.findByEmail(email);
    UUID verifyToken = UUID.randomUUID();
    authEntity.setEmailVerifyToken(verifyToken);
    this.authEntityRepository.save(authEntity);

    this.emailService.sendRequestEmailVerification(authEntity.getEmail(), verifyToken);
  }

  public UserDTO changeProfileImage(String url, UUID userId) {
    Optional<UserEntity> optionalUserEntity = this.userEntityRepository.findById(userId);
    UserEntity userEntity = optionalUserEntity.get();
    userEntity.setProfileImageUrl(url);
    UserEntity updatedUser = this.userEntityRepository.save(userEntity);
    return this.userConverter.convertEntity(updatedUser);
  }
}
